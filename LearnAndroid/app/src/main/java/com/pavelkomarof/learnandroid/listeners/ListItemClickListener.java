package com.pavelkomarof.learnandroid.listeners;

import android.view.View;

public interface ListItemClickListener {
    public void onItemClick(int position, View view);
}

