package com.pavelkomarof.mysite

import android.app.Application;
import com.onesignal.OneSignal


class App : Application() {
    @Override
    override fun onCreate() {
        super.onCreate()

        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}