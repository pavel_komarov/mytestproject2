package com.pavelkomarof.mysite

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import com.google.android.material.floatingactionbutton.FloatingActionButton

import com.google.android.material.navigation.NavigationView

import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    //переменная для хранения состояния доступности интернета networkAvailable
    var networkAvailable = false
    lateinit var mWebView : WebView
    //переменная для макета панели навигации
    lateinit var drawerLayout: DrawerLayout
    //создаем визуальный компонент
    lateinit var navView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        //Создадим и настроим переключатель, управляющий отображением значка-гамбургера в тулбаре
        val toogle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

       // Свяжем переключатель с панелью навигации
        drawerLayout.addDrawerListener(toogle)
        toogle.syncState()

        navView.setNavigationItemSelectedListener(this)


        var url = getString(R.string.website_url)
        //переменная для адреса формы обратной связи
        // буду переходить по этому адресу при нажатии Floating Action Button на главном экране приложения.
        var urlFeedback = getString(R.string.website_urlFeedBack)

        mWebView = findViewById(R.id.webView)
        val webSettings = mWebView.settings
        webSettings.javaScriptEnabled = true
        webSettings.setAppCacheEnabled(false)

        loadWebSite(mWebView, url, applicationContext)


        //SetColorSchemeResources. Это обеспечит разноцветные вращающиеся круги в прогрессбаре
        swipeRefreshLayout.setColorSchemeResources(R.color.colorRed, R.color.colorBlue, R.color.colorGreen )
        swipeRefreshLayout.apply {
            setOnRefreshListener {
                if (mWebView.url != null) url = mWebView.url
                loadWebSite(mWebView, url, applicationContext)
            }

            setOnChildScrollUpCallback { parent, child ->  mWebView.getScrollY() > 0}
        }


        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            loadWebSite(mWebView, urlFeedback, applicationContext)
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

    }

    //переходы по пунктам меню.
    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        //В теле метода при помощи конструкции выбора, в зависимости от идентификатора нажатого элемента меню,
        // вызываем метод загрузки нужной нам страницы.
        when (item.itemId) {
            R.id.nav_home -> {
                val url = getString(R.string.website_url)
                loadWebSite(mWebView, url, applicationContext)
            }
            R.id.nav_at -> {
                val url = getString(R.string.website_at)
                loadWebSite(mWebView, url, applicationContext)
            }
            R.id.nav_kotlin -> {
                val url = getString(R.string.website_kotlin)
                loadWebSite(mWebView, url, applicationContext)
            }
            R.id.nav_java -> {
                val url = getString(R.string.website_java)
                loadWebSite(mWebView, url, applicationContext)
            }
            R.id.nav_android -> {
                val url = getString(R.string.website_android)
                loadWebSite(mWebView, url, applicationContext)
            }
            R.id.nav_video -> {
                val url = getString(R.string.website_video)
                loadWebSite(mWebView, url, applicationContext)
            }
        }
        //В конце вызываем закрытие панели навигации.
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    //loadWebsite(…), будет принимать webview, адрес сайта  и контекст, и будет загружать сайт.
    private fun loadWebSite(mWebView: WebView, url: String, context: Context) {

        progressBar.visibility = View.VISIBLE
        networkAvailable = isNetworkAvailable(context)
        mWebView.clearCache(true)
        if (networkAvailable) {
            wvVisible(mWebView)
            mWebView.loadUrl(url)
            mWebView.webViewClient = MyWebViewClient()
        } else {
            wvGone(mWebView)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    //Функция wvVisible отображает webview и скрывает текстовое поле
    private fun wvVisible(mWebView: WebView) {
        mWebView.visibility = View.VISIBLE
        tvCheckConnection.visibility = View.GONE
    }

    //wvGone – наоборот, и скрывает прогрессбар
    private fun wvGone(mWebView: WebView) {
        mWebView.visibility = View.GONE
        tvCheckConnection.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    //isNetworkAvailable для проверки доступа к сети
    //чтобы предупреждения устаревших методов не отображались
    @Suppress("DEPRECATION")
    private fun isNetworkAvailable(context: Context) : Boolean {
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return if (Build.VERSION.SDK_INT > 22) {
                val an = cm.activeNetwork ?: return false
                val capabilities = cm.getNetworkCapabilities(an) ?: return false
                capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            } else {
                val a = cm.activeNetworkInfo ?: return false
                a.isConnected && (a.type == ConnectivityManager.TYPE_WIFI || a.type == ConnectivityManager.TYPE_MOBILE)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false

    }
    ////onLoadComplete(),  будет останавливать прогрессбары и скрывать их
    private fun onLoadComplete() {
        swipeRefreshLayout.isRefreshing = false
        progressBar.visibility = View.GONE
    }

    //Сторонние ссылки не должны отображаться в приложении, поскольку оно связано только с нашим сайтом.
    // Для этого нам нужно переопределить методы класса WebViewClient.
    private inner class MyWebViewClient : WebViewClient() {

        @RequiresApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?,request: WebResourceRequest?): Boolean {

            val url = request?.url.toString()
            return urlOverride(url)
        }


        override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
            return urlOverride(url)
        }

        private fun urlOverride(url: String): Boolean {
            progressBar.visibility = View.VISIBLE
            networkAvailable = isNetworkAvailable(applicationContext)

            if (networkAvailable) {
                if (Uri.parse(url).host == getString(R.string.website_domain)) return false
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
                onLoadComplete()
                return true
            } else {
                wvGone(webView)
                return false
            }
        }

        // в  классе MyWebViewClient переопределяем функцию onReceivedError(…), которая вызывается с ошибкой сервера
        @Suppress("DEPRECATION")
        override fun onReceivedError(view: WebView?, errorCode: Int, description: String?,failingUrl: String?) {
            super.onReceivedError(view, errorCode, description, failingUrl)
            if (errorCode == 0) {
                view?.visibility = View.GONE
                tvCheckConnection.visibility = View.VISIBLE
                onLoadComplete()
            }
        }

        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            onReceivedError(view, error!!.errorCode, error.description.toString(), request!!.url.toString())
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            onLoadComplete()
        }

    }


}